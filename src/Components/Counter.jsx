import React from 'react';

class Counter extends React.Component {
    state = { count : 0 }

    handlerIncreament = (isIncreament)=> {
        let {count} = this.state;

        if(isIncreament){
            this.setState({count: count+1})
        }else if(count > 0){
            this.setState({count: count-1})
        }
    }

    render() { 
        let {count} = this.state;
        return ( 
            <>  
                <h1>Counter Component</h1>
                <button className="btn btn-primary btn-sm" onClick={()=>this.handlerIncreament(false)}>Decreament</button>
                <span className={this.formatClass()}>{this.formatCount()}</span>
                <button className="btn btn-primary btn-sm" onClick={()=>this.handlerIncreament(true)}>Increament</button>
            </>
         );
    }

    formatCount(){
        let {count} = this.state;
        return count === 0 ? "Zero" : count;
    }

    formatClass(){
        let {count} = this.state;
        let setClass = 'p-2 m-3 badge badge-';
        setClass+= count === 0 ? "warning" : 'primary';
        return setClass;
    }
}
 
export default Counter;



