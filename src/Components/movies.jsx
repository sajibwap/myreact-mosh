import React from 'react';
import {getMovies} from '../services/fakeMovieService';

class Movies extends React.Component {
	state = {
		movies: getMovies(),
	};

	handleDelete = movie =>{
		const movies = this.state.movies.filter(m=>m._id !=movie._id);
		this.setState({movies})
	}

	render() {
		const count = this.state.movies.length;

		return (
			<>
			<h1>Moveis Component</h1>
			{count === 0 ? <h3 className='text-danger'>No movie found</h3> : 

			<React.Fragment>
			<p>Showing {count} movies from database</p>
			<table className="table">
				<thead className="bg-danger text-white">
					<tr>
						<th>Title</th>
						<th>Genre</th>
						<th>NumberInStock</th>
						<th>DailyRentalRate</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				{this.state.movies.map(movie=>(
					<tr key={movie._id}>
						<td>{movie.title}</td>
						<td>{movie.genre.name}</td>
						<td>{movie.numberInStock}</td>
						<td>{movie.dailyRentalRate}</td>
						<td>
							<button 
								onClick={()=>this.handleDelete(movie)} 
								className="btn btn-primary">Delete
							</button>
						</td>
					</tr>

					))}
				</tbody>
			</table>
			</React.Fragment>
			}
			</>
			);
	}
}

export default Movies;