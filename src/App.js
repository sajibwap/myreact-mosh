import Counter from "./Components/Counter";
import Movies from "./Components/movies";

function App() {
  return (
    <div className="App container">
      <header className="App-header my-5">
        <h1>Code with Mosh</h1>
      </header>
      <Counter />
      <hr />
      <Movies />
    </div>
  );
}

export default App;
